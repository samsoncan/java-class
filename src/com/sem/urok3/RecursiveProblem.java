package com.sem.urok3;

public class RecursiveProblem {

	public static void main(String[] args) {
		RecursiveProblem rp = new RecursiveProblem();
		rp.printHelo();
	}

	private void printHelo() {
		System.out.println("Hello");
		printBonjour();
		System.out.println("Terminating");
	}

	private void printBonjour() {
		System.out.println("Bonjoujur");
		printHelo();
	}
}
