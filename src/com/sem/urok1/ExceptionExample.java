package com.sem.urok1;

import java.io.File;
import java.io.IOException;

public class ExceptionExample {
	File f = new File("aaa.txt");





	public void readUserNumber0() {

		try {
			// something dangerous
			f.createNewFile();
			f.deleteOnExit();
			f.exists();
			// todo
		} catch (IOException ex) {
			// recover if possible
			ex.printStackTrace();

		} catch (Exception exx) {
			// show message to user
		} catch (Throwable all) {
			// tres grand problem
		}



	}
























	public void readUserNumber1() {
		try {
			f.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void readUserNumber2() {
		try {
			f.createNewFile();
		} catch (IOException | SecurityException e) {
			e.printStackTrace();
		} catch (Throwable ex) {
			System.exit(1);
		}
	}


	public void readUserNumber3() {
		try {
			f.createNewFile();
		} catch (SecurityException e) {
			System.out.print("Problem of permition for current user..");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.print("Catastrophique Problem terminating...");
			System.exit(1);
		}
	}

	public void run() throws IOException {
		readUserNumber0();
	}
}
