package com.sem.urok1;

public class Person implements GlobalPerson {

	public String name;
	public String familyName;

	public String getName() {
		return name;
	}

	public Person(String personName, String familyName) {
		name = personName;
		this.familyName = familyName;
	}

	@Override
	public String toString() {
		return "my name: " + name + " familyName: " + familyName;
	}

	@Override
	public boolean equals(Object obj) {
		Person right = (Person) obj;
		boolean yesequal = this.name.equals(right.name);
		boolean yesequal2 = this.familyName.equals(right.familyName);
		return yesequal & yesequal2;
	}
	// @Override
	// public boolean equals(Object other) {
	// if (other instanceof Person) {
	// Person otherPerson = (Person) other;
	// return otherPerson.name.equals(this.name) && otherPerson.familyName.equals(this.familyName);
	// }
	//
	// return false;
	// }

	@Override
	public String getFamilyName() {
		// TODO Auto-generated method stub
		return name + familyName;
	}
}
