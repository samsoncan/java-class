package com.sem.urok1;

public class If_while_dowhile {

	public void ifelse(int x) {

		if (x == 10) {
			System.out.println("ten");
		} else if (x == 100) {
			System.out.println("hundred");
		} else if (x == 1000) {
			System.out.println("thousand");
		} else if (x == 10000) {
			System.out.println("ten thousand");
		}
	}

	public void exwhile() {
		int i = 10;
		while (i > 0) {
			System.out.println("hello#" + i);
			i--;
		}
	}

	public void exdowhile() {

		int i = 10;
		do {
			System.out.println("hello#" + i);
			i--;
		} while (i > 0);
	}

}
