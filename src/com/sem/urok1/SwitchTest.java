package com.sem.urok1;

public class SwitchTest {

	public void run1(int x) {

		if (x == 10) {
			System.out.println("ten");
		} else if (x == 100) {
			System.out.println("hundred");
		} else if (x == 1000) {
			System.out.println("thousand");
		} else if (x == 10000) {
			System.out.println("ten thousand");
		}
	}

	public void run2(int x) {

		switch (x) {
		case 10:
			System.out.println("ten");
			break;
		case 100:
			System.out.println("hundred");
			break;
		case 1000:
			System.out.println("thousand");
			break;
		case 10000:
			System.out.println("ten thousand");
			break;
		default: {
			System.out.println("not in range....");
		}
		}
	}

}
