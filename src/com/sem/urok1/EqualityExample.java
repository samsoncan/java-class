package com.sem.urok1;

public class EqualityExample {

	//	public void primitive() {
	//
	//		int x = 5;
	//		int y = 10;
	//
	//		if (x == 4) {
	//			System.out.println("Equal");
	//		} else {
	//			System.out.println("NOT Equal");
	//		}
	//	}

	public void reference() {

		Person p1 = new Person("Antonio", "Samson");
		Person p2 = new Person("Antonio", "Samson");
		boolean isequal = p1.equals(p2);

		if (isequal) {
			System.out.println("Equal");
		}
		else {
			System.out.println("NOT Equal");
		}
	}
}
