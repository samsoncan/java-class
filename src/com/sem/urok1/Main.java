package com.sem.urok1;

import java.util.List;

import com.sem.urok4.generics.NotGenericExample;

public class Main {

	static String[] names = new String[] { "Dave", "David", "Dian", "Dan" };
	// static Long[] ages = new Long[] { 23L, 34L, 26L, 43L };
	// static Point[] coords = new Point[] { new Point(3, 10), new Point(0, 1), new Point(20, 5), };

	public static void main(String[] args) {

		// not Generic way
		List<String> nameList = NotGenericExample.array2List(names);
		System.out.println(nameList);
		//
		// List<Long> agesList = NotGenericExample.array2List(ages);
		// System.out.println(agesList);
		//
		// List<Point> coordsList = NotGenericExample.array2List(coords);
		// System.out.println(coordsList);

		// Generic way
		// List<String> nameList2 = GenericExample.array2List(names);
		// System.out.println(nameList2);
		//
		// List<Long> agesList2 = GenericExample.array2List(ages);
		// System.out.println(agesList2);
		//
		// List<Point> coordsList2 = GenericExample.array2List(coords);
		// System.out.println(coordsList2);

		// testMaxLength();
	}


	// public static void testMaxLength() {
	// Integer[][] arr2dim = new Integer[3][];
	// arr2dim[0] = new Integer[] { 1, 2, 3 };
	// arr2dim[1] = new Integer[] { 1, 2, 3, 2, 3, 3 };
	// arr2dim[2] = new Integer[] { 1, 2, 3 };
	//
	// int maxLen = OtherGenericsMethods.maxLength(arr2dim);
	// System.out.println("max length=" + maxLen);
	// }


}
