package com.sem.urok1;

public class PassByReference {

	public void run() {

		Person president = new Person("Trump", "Body");
		System.out.println(president.name);
		change(president);
		System.out.println(president.name);

		int x = 5;
		System.out.println(x);
		change(x);
		System.out.println(x);
	}

	public void change(Person x) {
		x.name = "Obama";
	}

	public void change(int x) {
		x = 100;
	}

}
