package com.sem.urok4.generics;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

public class NotGenericExample {

	public static List<String> array2List(String[] strArr) {

		// create resulting list
		List<String> result = new LinkedList<>();

		for (int i = 0; i < strArr.length; i++) {
			result.add(strArr[i]);
		}

		return result;
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	public static List<Point> array2List(Point[] strArr) {

		// create resulting list
		List<Point> result = new LinkedList<>();

		for (int i = 0; i < strArr.length; i++) {
			result.add(strArr[i]);
		}

		return result;
	}

	/*
	 * 
	 *
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	public static List<Long> array2List(Long[] strArr) {

		// create resulting list
		List<Long> result = new LinkedList<>();

		for (int i = 0; i < strArr.length; i++) {
			result.add(strArr[i]);
		}

		return result;
	}
}
