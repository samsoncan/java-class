package com.sem.urok4.generics;

public class OtherGenericsMethods {

	public static <T> int maxLength(T[][] twoDimArr) {
		int maxLength = 0;

		for (int i = 0; i < twoDimArr.length; i++) {
			T[] temp = twoDimArr[i];
			if (maxLength < temp.length) {
				maxLength = temp.length;
			}
		}
		return maxLength;
	}
}
