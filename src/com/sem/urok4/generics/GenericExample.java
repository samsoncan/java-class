package com.sem.urok4.generics;

import java.util.LinkedList;
import java.util.List;

public class GenericExample {

	public static <T> List<T> array2List(T[] strArr) {

		// create resulting list
		List<T> result = new LinkedList<>();

		for (int i = 0; i < strArr.length; i++) {
			result.add(strArr[i]);
		}

		return result;
	}

}
