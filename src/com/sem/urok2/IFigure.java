package com.sem.urok2;

public interface IFigure {

	public int getPerimeter();

	public int getArea();
}
