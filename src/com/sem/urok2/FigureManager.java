package com.sem.urok2;

public class FigureManager {

	public int sumPerimeters(Square figure1, Triangle figure2) {
		return figure1.getPerimeter() + figure2.getPerimeter();
	}

	public int sumPerimeters(Diamond figure1, Triangle figure2) {
		String[] arr = new String[10];

		return figure1.getPerimeter() + figure2.getPerimeter();
	}

	public int sumPerimeters(Diamond figure1, Triangle figure2, Square figure3) {
		return figure1.getPerimeter() + figure2.getPerimeter() + figure3.getPerimeter();
	}

	public int sumPerimeter(IFigure f1, IFigure f2) {
		return f1.getPerimeter() + f1.getPerimeter();
	}

	public int sumAreas(IFigure[] arrayOfFigures) {

		int result = 0;
		for (int i = 0; i < arrayOfFigures.length; i++) {
			result += arrayOfFigures[i].getArea();
		}
		return result;
	}
}
