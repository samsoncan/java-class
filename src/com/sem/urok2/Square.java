package com.sem.urok2;

public class Square implements IFigure {

	private int base;

	@Override
	public int getPerimeter() {
		return base * 4;
	}

	@Override
	public int getArea() {
		return base * base;
	}
}
