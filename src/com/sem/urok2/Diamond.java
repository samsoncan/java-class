package com.sem.urok2;

public class Diamond implements IFigure {

	private int side;

	@Override
	public int getPerimeter() {
		return 4 * side;
	}

	@Override
	public int getArea() {
		return side * side;
	}

}
